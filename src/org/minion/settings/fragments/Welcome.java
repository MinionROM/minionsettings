package org.minion.settings.fragments;

import android.app.Activity;
import org.minion.settings.SettingsPreferenceFragment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;
import android.os.Bundle;

import org.minion.settings.R;

public class Welcome extends SettingsPreferenceFragment implements OnPreferenceChangeListener {

    private static final String TAG = "Welcome to MinionROM";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_welcome);

        addPreferencesFromResource(R.xml.welcome_settings);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        return true;
    }
}
